var express = require('express');
var app = express();
var port = process.env.port || 8443;
app.use('/bower_components', express.static('bower_components'));
app.use('/', express.static('web'));
console.log("express listening to port: " + port );
app.listen(port);