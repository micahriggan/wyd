angular.module('wyd')
    .controller('GoalController', function($scope) {


        $scope.rootGoal = {
            name: 'Goals',
            description: 'Goalname, Goal description, current step',
            locked : true,
            goals: [{
                name: 'Get this site working',
                description: 'This site is for logging of goals and the steps it takes to complete them',
                weight: 100,
                completed: false,
                type: 'code',
                log: [],
                goals: [{
                    name: 'Get goal panel working',
                    description: 'The goal panel should allow creation of goals with steps (which are nested goals)',
                    weight: 20,
                    completed: false,
                    type: 'code',
                    log: []
                }, {
                    name: 'Get prompts working',
                    description: 'The prompts panel should allow creation of prompts with timeframes that they will be triggered',
                    weight: 20,
                    completed: false,
                    type: 'code',
                    log: []
                }]
            }]
        };
        $scope.current = $scope.rootGoal;
        $scope.nextToDo = nextToDo;
        $scope.expand = expand;
        $scope.onRootGoal = onRootGoal;
        $scope.addGoal = addGoal;
        $scope.subgoalweight = subgoalweight;
        
        function nextToDo(goal) {
            console.log(goal.goals);
            return _.findWhere(goal.goals, {
                completed: false
            });
        }

        function expand(goal) {
            if (goal) {
                goal.previous = $scope.current;
                $scope.current = goal;
            }
        }

        function onRootGoal() {
            return $scope.current == $scope.rootGoal;
        }
        
        function addGoal(){
            console.log($scope.current);
            console.log($scope.newGoal);
            $scope.current.goals = ($scope.current.goals || []);
            if($scope.newGoal)
                $scope.current.goals.push(new Goal($scope.newGoal));
            $scope.newGoal = {};
        }
        
        function subgoalweight(goal){
            return _.reduce(goal.goals || [], function(memo, currentGoal){
                var subgoalTotal = (currentGoal.goals || []).length ? subgoalweight(currentGoal) : 0; 
                var completed = currentGoal.completed || false;
                return memo + (currentGoal.weight || 0) * completed + subgoalTotal ; 
            }, 0);               
        }
        
        function Goal(obj){
            return {
                name: obj.name || '',
                description: obj.description || '',
                weight: obj.weight || 0,
                completed: obj.completed || false,
                type: obj.type ||  '',
                log: obj.log || [],
                goals: obj.goals ||  []
            }
        }

    });